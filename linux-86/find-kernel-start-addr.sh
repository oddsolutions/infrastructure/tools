#!/bin/bash
for addr in $(xxd -p "${1}" | tr "\n" " " | sed "s/ //g" | sed "s/c0ffffff/c0ffffff\n/g" | grep -o '.\{16\}$'); do echo ${addr:14:2}${addr:12:2}${addr:10:2}${addr:8:2}${addr:6:2}${addr:4:2}${addr:2:2}${addr:0:2}; done | sort
