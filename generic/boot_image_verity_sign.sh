#!/bin/bash
boot_sign /boot ${1} "${HOME}"/lineage-20/build/make/target/product/security/verity.pk8 "${HOME}"/lineage-20/build/make/target/product/security/verity.x509.pem ${1}
boot_sign /recovery ${1} "${HOME}"/lineage-20/build/make/target/product/security/verity.pk8 "${HOME}"/lineage-20/build/make/target/product/security/verity.x509.pem ${1}
