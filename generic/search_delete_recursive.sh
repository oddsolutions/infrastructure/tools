#!/bin/bash

# Check if a search pattern is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <file-pattern>"
    exit 1
fi

# Find files and directories matching the pattern
items=$(find . -name "$1")

# Check if any items were found
if [ -z "$items" ]; then
    echo "No files or directories found matching the pattern: $1"
    exit 0
fi

# Display found items
echo "The following files and/or directories will be deleted:"
echo "$items"
echo
read -p "Are you sure you want to delete these items? (y/N): " confirm

# If user confirms, delete the items
if [[ "$confirm" =~ ^[Yy]$ ]]; then
    echo "$items" | xargs rm -rv
    echo "Deletion complete."
else
    echo "Operation canceled."
fi
