#!/bin/bash
# quick and dirty bash script to extract .gnu_debugdata section
# from ELF binaries to generate an IDC script that adds these
# names as symbols
# --rpw, 2020-06-21

SYMBOLFILE=debugdata_symbols.elf

if [ $# -lt 1 ]; then
	echo "you need to supply a path to a binary"
	exit 1
fi

bin="$1"
idcname="$1"-symbols.idc

objdump -h -j .gnu_debugdata "$bin" > /dev/null 2> /dev/null

if [ $? -ne 0 ]; then
	echo "section .gnu_debugdata was not found in binary \"$bin\""
	exit 1
fi

if [ "$ARCH""x" != "x" -a "${ARCH: -1}""x" != "-x" ]; then
	ARCH="$ARCH""-"
fi

rm -f $SYMBOLFILE{,.xz}
${ARCH}objcopy --dump-section .gnu_debugdata=$SYMBOLFILE.xz $bin 2> /dev/null > /dev/null

if [ $? -ne 0 ]; then
	echo "The .gnu_debugdata section exists, but I an unable to extract it. You may need to"
	echo "specify an ARCH env variable (e.g. export ARCH=aarch64-linux-gnu-). This requires"
	echo "a copy of \$ARCH-objcopy in your \$PATH. Here's readelf's opinion on your binary: " 
	echo 
	readelf -h "$bin"|egrep 'Class|OS\/ABI|Machine'
	exit 1
fi

xz -d $SYMBOLFILE.xz
cat > $idcname << EOF
#include <idc.idc>

static main() {
EOF
nm $SYMBOLFILE|awk '{print "  set_name(0x"$1", \""$3"\");" }' >> $idcname
echo "}" >> $idcname
rm -f $SYMBOLFILE
