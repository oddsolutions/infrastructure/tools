#!/bin/bash

# Input Variables
ips=${1}
passwords=${2}
usernames="admin nolenjohnson root ''"

script_date=$(date +%Y%m%d)

for ip in ips; do
  echo "-------- $ip --------">>"${PWD}"/ssh-check.log
  for username in usernames; do
    for password in passwords; do
      sshpass -p "$password" ssh -o ConnectTimeout=10 "$username"@"$ip" 'uname -a'
      return_code="${!}"
      echo !:*>>"${PWD}"/ssh-check.log
      echo "${return_code}">>"${PWD}"/ssh-check.log
      echo "-------- /$ip --------">>"${PWD}"/ssh-check-"${script_date}".log
      echo "">>"${PWD}"/ssh-check.log
    done
  done
done
