#!/bin/bash
VENDOR_SEPOL_PATH="${1}"
OUT_DIR="${2}"
​
rm -rf contexts contexts-sorted vendor-contexts unfinished-types unfinished-types-sorted ${OUT_DIR}
mkdir -p ${OUT_DIR}/vendor
​
context_files="file_contexts hwservice_contexts property_contexts seapp_contexts"
for context_file in $context_files; do
    while read context_line; do
        if ! grep -Frq "`echo "${context_line}" | cut -f1`" ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat; then
            echo ${context_line} >> ${OUT_DIR}/vendor/${context_file}
        fi
    done < ${VENDOR_SEPOL_PATH}/nonplat_${context_file}
done
​
# vndservice_contexts is special...
while read context_line; do
    if ! grep -Frq "`echo "${context_line}" | cut -f1`" ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat; then
        echo ${context_line} >> ${OUT_DIR}/vendor/service_contexts
    fi
done < ${VENDOR_SEPOL_PATH}/vndservice_contexts
​
# genfs_contexts
while read line; do
    if echo "$line" | grep -q "(genfscon"; then
        fsype=$(echo "$line" | cut -d' ' -f2)
        path=$(echo "$line" | cut -d' ' -f3)
        type=$(echo "$line" | cut -d' ' -f6)
        if ! grep -rq "${path}" ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat; then
            echo -e "genfscon ${fsype} ${path} u:object_r:${type}:s0" >> ${OUT_DIR}/vendor/genfs_contexts
        fi
    fi
done < ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil
​
### file.te ###
​
grep -v '^#' ${OUT_DIR}/vendor/file_contexts ${OUT_DIR}/vendor/genfs_contexts | while read line; do
   echo $line | sed 's#^.*\(u:object_r.*\)#\1#g' | sed 's#u:object_r:##g' | sed 's#:s0##g' >> contexts
done
​
sort contexts | uniq > contexts-sorted
​
while read context; do
   grep -Frq "type ${context}," ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat || echo $context >> vendor-contexts
done < contexts-sorted
​
while read type; do
    if grep -q "(typealiasactual ${type}" ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil; then
        aliasarray=($(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typealiasactual*" | grep " ${type} " | sed 's#^(typealiasactual ##g' | sed 's#(.*##g' | sort | sed 's#)##'))
        echo "`echo type ${aliasarray[1]}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${aliasarray[1]} " | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/file.te
       	echo "`echo typealias ` `echo ${aliasarray[1]}` `echo "alias"` `echo ${aliasarray[0]}`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/file.te
    else
        echo "`echo type ${type}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${type} \|(${type} \| ${type})" | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/file.te
    fi
done < vendor-contexts
​
rm -rf contexts contexts-sorted vendor-contexts
​
### hwservice ###
​
grep -v '^#' ${OUT_DIR}/vendor/hwservice_contexts | while read line; do
   echo $line | sed 's#^.*\(u:object_r.*\)#\1#g' | sed 's#u:object_r:##g' | sed 's#:s0##g' >> contexts
done
​
sort contexts | uniq > contexts-sorted
​
while read context; do
   grep -Frq "type ${context}," ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat --include=*\.te || echo $context >> vendor-contexts
done < contexts-sorted
​
while read type; do
    if grep -q "(typealiasactual ${type}" ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil; then
        aliasarray=($(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typealiasactual*" | grep " ${type} " | sed 's#^(typealiasactual ##g' | sed 's#(.*##g' | sort | sed 's#)##'))
        echo "`echo type ${aliasarray[1]}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${aliasarray[1]} " | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/hwservice.te
       	echo "`echo typealias ` `echo ${aliasarray[1]}` `echo "alias"` `echo ${aliasarray[0]}`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/hwservice.te
    else
        echo "`echo type ${type}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${type} \|(${type} \| ${type})" | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/hwservice.te
    fi
    out_file=$(echo ${type} | sed 's#_hwservice#_default#')
    echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(allow $(echo ${type} | sed 's#_hwservice##') " | sed 's#^(##' | sed 's# (#:#' | sed 's# (# { #' | sed 's#)))# };#') | sed 's#_28_0##g' | sed 's# allow#\nallow#g' >> ${OUT_DIR}/vendor/${out_file}.te
    echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(allow $(echo ${type} | sed 's#_hwservice#_default#') " | sed 's#^(##' | sed 's# (#:#' | sed 's# (# { #' | sed 's#)))# };#') | sed 's#_28_0##g' | sed 's# allow#\nallow#g' >> ${OUT_DIR}/vendor/${out_file}.te
    if grep -vq '[^[:space:]]' ${OUT_DIR}/vendor/${out_file}.te; then
        rm -rf ${OUT_DIR}/vendor/${out_file}.te
    fi
done < vendor-contexts
​
rm -rf contexts contexts-sorted vendor-contexts
​
### property.te ###
​
# in addition to skipping commented lines, we also don't want to re-define exported#_default_prop
grep -Ev '^#|exported' ${OUT_DIR}/vendor/property_contexts | while read line; do
   echo $line | sed 's#^.*\(u:object_r.*\)#\1#g' | sed 's#u:object_r:##g' | sed 's#:s0##g' >> contexts
done
​
sort contexts | uniq > contexts-sorted
​
while read context; do
   grep -nrq "type ${context}," ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat || echo $context >> vendor-contexts
done < contexts-sorted
​
while read type; do
    if grep -q "(typealiasactual ${type}" ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil; then
        aliasarray=($(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typealiasactual*" | grep " ${type} " | sed 's#^(typealiasactual ##g' | sed 's#(.*##g' | sort | sed 's#)##'))
        echo "`echo type ${aliasarray[1]}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${aliasarray[1]} " | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/property.te
       	echo "`echo typealias ` `echo ${aliasarray[1]}` `echo "alias"` `echo ${aliasarray[0]}`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/property.te
    else
        echo "`echo type ${type}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${type} \|(${type} \| ${type})" | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/property.te
    fi
done < vendor-contexts
​
rm -rf contexts contexts-sorted vendor-contexts
​
### service ###
​
grep -v '^#' ${OUT_DIR}/vendor/service_contexts | while read line; do
   echo $line | sed 's#^.*\(u:object_r.*\)#\1#g' | sed 's#u:object_r:##g' | sed 's#:s0##g' >> contexts
done
​
sort contexts | uniq > contexts-sorted
​
while read context; do
   grep -Frq "type ${context}," ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat --include=*\.te || echo $context >> vendor-contexts
done < contexts-sorted
​
while read type; do
    if grep -q "(typealiasactual ${type}" ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil; then
        aliasarray=($(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typealiasactual*" | grep " ${type} " | sed 's#^(typealiasactual ##g' | sed 's#(.*##g' | sort | sed 's#)##'))
        echo "`echo type ${aliasarray[1]}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${aliasarray[1]} " | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/service.te
       	echo "`echo typealias ` `echo ${aliasarray[1]}` `echo "alias"` `echo ${aliasarray[0]}`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/service.te
    else
        echo "`echo type ${type}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${type} \|(${type} \| ${type})" | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/service.te
    fi
    echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(allow ${type}" | sed 's#^(##' | sed 's# (#:#' | sed 's# (# { #' | sed 's#)))# };#') | sed 's#_28_0##g' | sed 's# allow#\nallow#g' >> ${OUT_DIR}/vendor/${type}.te
    if grep -vq '[^[:space:]]' ${OUT_DIR}/vendor/${type}.te; then
        rm -rf ${OUT_DIR}/vendor/${type}.te
    fi
done < vendor-contexts
​
rm -rf contexts contexts-sorted vendor-contexts
​
### app domains ###
​
grep -v '^#' ${OUT_DIR}/vendor/seapp_contexts | while read line; do
   echo $line | sed 's#^.*domain=##g' | sed 's# .*##g' >> contexts
done
​
sort contexts | uniq > contexts-sorted
​
while read context; do
   grep -Frq "type ${context}," ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat || echo $context >> vendor-contexts
done < contexts-sorted
​
while read type; do
    if grep -q "(typealiasactual ${type}" ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil; then
        aliasarray=($(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typealiasactual*" | grep " ${type} " | sed 's#^(typealiasactual ##g' | sed 's#(.*##g' | sort | sed 's#)##'))
        echo "`echo type ${aliasarray[1]}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${aliasarray[1]} " | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/${type}.te
       	echo "`echo typealias ` `echo ${aliasarray[1]}` `echo "alias"` `echo ${aliasarray[0]}`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/${type}.te
    else
        echo "`echo type ${type}, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " ${type} \|(${type} \| ${type})" | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/${type}.te
    fi
    echo -e "\n" >> ${OUT_DIR}/vendor/${type}.te
    echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(allow ${type}" | sed 's#^(##' | sed 's# (#:#' | sed 's# (# { #' | sed 's#)))# };#') | sed 's#_28_0##g' | sed 's# allow#\nallow#g' >> ${OUT_DIR}/vendor/${type}.te
    while read policy_line; do
        target=$(echo $policy_line | sed 's#:.*##g' | cut -d' ' -f3)
        if ! grep -Frq $target ../../../system/sepolicy/ ../../qcom/sepolicy --exclude-dir=prebuilts --exclude-dir=compat; then
            echo $target >> unfinished-types
        fi
    done < ${OUT_DIR}/vendor/${type}.te
done < vendor-contexts
​
sort unfinished-types | uniq > unfinished-types-sorted
sed -ir '/^\s*$/d' unfinished-types-sorted
while read target_context; do
    echo "`echo type $target_context, ` `echo $(cat ${VENDOR_SEPOL_PATH}/nonplat_sepolicy.cil | grep "^(typeattributeset*" | grep " $target_context " | sed 's#^(typeattributeset ##g' | sed 's#(.*##g' | sort) | sed 's# #, #g'`" | sed 's#$#;#g' >> ${OUT_DIR}/vendor/extras.te
done < unfinished-types-sorted
​
rm -rf contexts contexts-sorted vendor-contexts unfinished-types unfinished-types-sorted
