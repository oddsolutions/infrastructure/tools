#!/bin/bash

SOURCE=$1

PRODUCT_MANUFACTURER=$(cat $SOURCE/system/build.prop | grep ro.product.system.manufacturer= | cut -c 32-)
SYSTEM_NAME=$(cat $SOURCE/system/build.prop | grep ro.product.system.name= | cut -c 24-)
SYSTEM_DEVICE=$(cat $SOURCE/system/build.prop | grep ro.product.system.device= | cut -c 26-)
VERSION_RELEASE=$(cat $SOURCE/system/build.prop | grep ro.system.build.version.release= | cut -c 33-)
VENDOR_BUILD_ID=$(cat $SOURCE/vendor/build.prop | grep ro.vendor.build.id= | cut -c 20-)
VENDOR_VERSION_INCREMENTAL=$(cat $SOURCE/vendor/build.prop | grep ro.vendor.build.version.incremental= | cut -c 37-)
SYSTEM_VERSION_INCREMENTAL=$(cat $SOURCE/system//build.prop | grep ro.system.build.version.incremental= | cut -c 37-)
SYSTEM_BUILD_TYPE=$(cat $SOURCE/system/build.prop | grep ro.system.build.type= | cut -c 22-)
SYSTEM_BUILD_TAGS=$(cat $SOURCE/system/build.prop | grep ro.system.build.tags= | cut -c 22-)

SECURITY_PATCH=$(cat $SOURCE/system/build.prop | grep ro.build.version.security_patch= | cut -c 33-)

echo $SYSTEM_NAME-$SYSTEM_BUILD_TYPE $VERSION_RELEASE $VENDOR_BUILD_ID $VENDOR_VERSION_INCREMENTAL-$SYSTEM_VERSION_INCREMENTAL $SYSTEM_BUILD_TAGS

echo $PRODUCT_MANUFACTURER/$SYSTEM_NAME/$SYSTEM_DEVICE:$VERSION_RELEASE/$VENDOR_BUILD_ID/$VENDOR_VERSION_INCREMENTAL-$SYSTEM_VERSION_INCREMENTAL:$SYSTEM_BUILD_TYPE/$SYSTEM_BUILD_TAGS
echo
echo security patch: $SECURITY_PATCH
