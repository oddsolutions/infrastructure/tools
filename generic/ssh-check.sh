#!/bin/bash

# Input Variables
#ips=${1}
ips=$(cat "${PWD}"/ips.txt)
#passwords=${2}
passwords="admin administrator cusadmin password root moxa"
usernames="admin administrator root user cisco"

script_date=$(date +%Y%m%d)
log="${PWD}"/ssh-check-"${script_date}".log

for ip in ${ips}; do
  echo "-------- Start: $ip --------">>"${log}"
  for username in ${usernames}; do
    for password in ${passwords}; do
      echo "Command:" sshpass -p "$password" ssh -o ConnectTimeout=5 "$username"@"$ip" 'uname -a'>>"${log}"
      sshpass -p "$password" ssh -o ConnectTimeout=10 -o StrictHostKeyChecking=no "$username"@"$ip" 'uname -a'>>"${log}"
#      sshpass -p "$password" ssh -c aes128-ctr -o PubkeyAcceptedAlgorithms=+ssh-rsa -o KexAlgorithms=+diffie-hellman-group1-sha1 -o HostkeyAlgorithms=+ssh-rsa -o ConnectTimeout=10 -o StrictHostKeyChecking=no "$username"@"$ip" `uname -a`>>"${log}"
      return_code="${?}"
      echo "Username: '$username'">>"${log}"
      echo "Password: '$password'">>"${log}"
      if [ "${return_code}" != 0 ]; then
          echo "Failed: Status Code ${return_code}">>"${log}"
        else
          echo "Succeeded: Status Code ${return_code}">>"${log}"
      fi
      echo "">>"${log}"
    done
  done
  echo "-------- End: $ip --------">>"${log}"
done
